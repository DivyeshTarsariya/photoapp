package com.han.photoapp;

/*
 * Default Main Screen Class
 */

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import com.han.utility.CameraUtility;
import com.han.utility.DialogUtility;
import com.han.utility.ImageUtility;
import com.han.utility.ShareUtility;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends ActionBarActivity {

	Button btnBW;
	Button btnGray;
	Button btnBright;
	
	Button btnSave;
	Button btnTake;
	Button btnSend;
	
	ImageView imgPhoto;
	
	Bitmap takenBmp = null;
	Bitmap resBmp = null;
	
	private Uri fileUri;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		btnBW = (Button) findViewById(R.id.bw_button);
		btnGray = (Button) findViewById(R.id.gray_button);
		btnBright = (Button) findViewById(R.id.bright_button);
		
		btnSave = (Button) findViewById(R.id.save_button);
		btnTake = (Button) findViewById(R.id.take_button);
		btnSend = (Button) findViewById(R.id.send_button);
		
		imgPhoto= (ImageView) findViewById(R.id.photo_imageView);
	}
	
	private void initValue() {
		takenBmp = BitmapFactory.decodeResource(getResources(), R.drawable.original);
		resBmp = takenBmp;
	}
	
	//  take photo from camera
	public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 
        fileUri = CameraUtility.getOutputMediaFileUri(CameraUtility.MEDIA_TYPE_IMAGE);
        
        try {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } catch (Exception e) {
        	e.printStackTrace();
        }
 
        startActivityForResult(intent, CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
	
	//  select photo from gallery
	public void selectPhoto() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, CameraUtility.SELECT_PICTURE_REQUEST_CODE);
	}
	
	// handle get picture
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturedIntent) {
		if (requestCode == CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			
			// when take picture from camera
            if (resultCode == RESULT_OK) { 	
            	previewImage();
            }
        }
		if (requestCode == CameraUtility.SELECT_PICTURE_REQUEST_CODE) {
			
			// when select picture from gallery
			if (resultCode == RESULT_OK) {
				fileUri = imageReturedIntent.getData();
				fileUri = ImageUtility.getRealUriFromContentUri(fileUri);
				previewImage();
			}
		}
	}
	
	// preview image taken 
	private void previewImage() {
		Log.e("Image File path", fileUri.getPath());
				
		takenBmp = ImageUtility.getBitmapFromFileUri(fileUri, 0, 0);

		if (takenBmp != null) {
			imgPhoto.setImageBitmap(takenBmp);
		}
    }
	
	private void initEvent() {
		btnBW.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				// click black/white button
				if (takenBmp == null) {
					DialogUtility.show(MainActivity.this, "Please take picture.");
					return;
				}
				
				resBmp = ImageUtility.convertColorBlackWhite(takenBmp);
				
				imgPhoto.setImageBitmap(resBmp);
			}
        });
		btnGray.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				// click gray-level button
				if (takenBmp == null) {
					DialogUtility.show(MainActivity.this, "Please take picture.");
					return;
				}
				
				resBmp = ImageUtility.convertSepia(takenBmp);
				imgPhoto.setImageBitmap(resBmp);
			}
        });
		btnBright.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				// click brightness button
				if (takenBmp == null) {
					DialogUtility.show(MainActivity.this, "Please take picture.");
					return;
				}
				
//				int brightnessLevel = sbBrightnessLevel.getProgress() - 255;
				resBmp = ImageUtility.convertBrightness(takenBmp, 60);
				imgPhoto.setImageBitmap(resBmp);
			}
        });
		btnSave.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				// click save button
				String root = Environment.getExternalStorageDirectory().toString();
				File myDir = new File(root + "/saved_images");    
				myDir.mkdirs();
				Random generator = new Random();
				int n = 10000;
				n = generator.nextInt(n);
				String fname = "saved_image.jpg";
				File file = new File (myDir, fname);
				if (file.exists ()) file.delete (); 
				try {
				       FileOutputStream out = new FileOutputStream(file);
				       resBmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
				       out.flush();
				       out.close();

				} catch (Exception e) {
				       e.printStackTrace();
				}
			}
        });
		btnTake.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				// click take button
				DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
					
					private static final int TAKE_PICTURE = 0;
					private static final int SELECT_PICTURE = 1;
					private static final int CANCEL = 2;
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case TAKE_PICTURE:
								Log.e("TAKE_PICTURE", "click");
								takePhoto();
								break;
							case SELECT_PICTURE:
								Log.e("SELECT_PICTURE", "click");
								selectPhoto();
								break;
							case CANCEL:
				    			dialog.dismiss();
				    			break;
						}
					}
				};
				
				new AlertDialog.Builder(MainActivity.this)
			    	.setItems(R.array.dialog_get_picture, listener)
			    	.setCancelable(true)
			    	.show();
			}
        });
		btnSend.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
				final File file = new File(extStorageDirectory, "send.png");
				writeImageBeforeSave(file);
				
				ShareUtility.share(MainActivity.this, file);
			}
        });
//		sbGrayScaleLevel.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
//
//			@Override
//			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//
//				int grayScalLevel = seekBar.getProgress();
////				imgPhoto.setImageBitmap(ImageUtility.convertGrayScale(takenBmp, grayScalLevel));
//			}
//
//			@Override
//			public void onStartTrackingTouch(SeekBar seekBar) {
//				// TODO Auto-generated method stub
//			}
//
//			@Override
//			public void onStopTrackingTouch(SeekBar seekBar) {
//				// TODO Auto-generated method stub
//			}     
//		});
//		sbBrightnessLevel.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
//
//			@Override
//			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//				int brightnessLevel = sbBrightnessLevel.getProgress() - 255;
//				
//				imgPhoto.setImageBitmap(ImageUtility.convertBrightness(takenBmp, brightnessLevel));
//			}
//
//			@Override
//			public void onStartTrackingTouch(SeekBar seekBar) {
//				// TODO Auto-generated method stub
//			}
//
//			@Override
//			public void onStopTrackingTouch(SeekBar seekBar) {
//				// TODO Auto-generated method stub
//			   
//			}     
//		});
	}
	
	// create file before sending image
	public void writeImageBeforeSave(File file) {
		if (resBmp == null) {
			DialogUtility.show(this, "Please take image");
			return;
		}
		try {
			FileOutputStream outStream = new FileOutputStream(file);
			resBmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
		    outStream.flush();
		    outStream.close();
		} catch (Exception e) {
			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
