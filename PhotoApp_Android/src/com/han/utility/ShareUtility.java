package com.han.utility;

/*
 * This class is one for send image
 */
import java.io.File;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
  
public class ShareUtility {
	
	// send image
	public static boolean share(Context ctx, File file) {
		try {
			Intent shareIntent = new Intent();
			shareIntent.setAction(Intent.ACTION_SEND);
			shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
			shareIntent.setType("image/jpeg");
			ctx.startActivity(Intent.createChooser(shareIntent, "Share image using"));
			
			return true;
		} catch(Exception e) {
		  	e.printStackTrace();
			return false;
		}
	}
}
