package com.han.utility;

/*
 * Class for show simple Toast
 */

import android.content.Context;
import android.widget.Toast;

public class DialogUtility {

    private static Toast toast;
	
    // show short Toast;
	public static void show(Context context, String message) {
        if (message == null) {
            return;
        }
        if (toast == null && context != null) {
            toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        }
        if (toast != null) {
            toast.setText(message);
            toast.show();
        }
	}
}
