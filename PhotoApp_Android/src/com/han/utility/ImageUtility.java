package com.han.utility;

/*
 * This class is library for processing image  
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import com.han.photoapp.App;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

public class ImageUtility {
	
	// get rotated image bitmap
	public static Bitmap getRotatedBitmap(Bitmap bmpSrc, int orientation) {

		Matrix matrix = new Matrix();

        switch (orientation) { 
        	case ExifInterface.ORIENTATION_ROTATE_90:
        		matrix.postRotate(90);
        		break;
        	case ExifInterface.ORIENTATION_ROTATE_180:
        		matrix.postRotate(180);
        		break;
        	case ExifInterface.ORIENTATION_ROTATE_270:
        		matrix.postRotate(270);
        		break;
        }
        
        Bitmap newBmp = Bitmap.createBitmap(bmpSrc, 0, 0,
        		bmpSrc.getWidth(), bmpSrc.getHeight(),
                matrix, true);
        
        return newBmp;
	}
	
	// get bitmap from file uri
	public static Bitmap getBitmapFromFileUri(Uri fileUri, int width, int height) {
		
		if (fileUri == null) return null;
		
		try {
			Bitmap takenBmp = decodeFile(fileUri.getPath());
			
			if (width == 0 && height == 0) {
				takenBmp = Bitmap.createScaledBitmap(takenBmp, takenBmp.getWidth(), takenBmp.getHeight(), true);
			} else {
				takenBmp = Bitmap.createScaledBitmap(takenBmp, width, height, true);	
			}

			ExifInterface ei;
	        ei = new ExifInterface(fileUri.getPath());
	        
	        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
	                ExifInterface.ORIENTATION_NORMAL);
	        
    		takenBmp = getRotatedBitmap(takenBmp, orientation);
    		
    		return takenBmp;
	    } catch (IOException e) {
	        e.printStackTrace();
			return null;
	    }
	}
	
	// get file path from file when selected picture from gallery
	public static String getRealPathFromContentUri(Uri contentUri) {
	    String result;
	    Cursor cursor = App.getInstance().getContentResolver().query(contentUri, null, null, null, null);
	    if (cursor == null) { // Source is Dropbox or other similar local file path
	        result = contentUri.getPath();
	    } else {
	        cursor.moveToFirst(); 
	        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
	        result = cursor.getString(idx);
	        cursor.close();
	    }
	    
	    Log.e("real path", result);
	    return result;
	}
	
	// get file url from file when selected picture from gallery
	public static Uri getRealUriFromContentUri(Uri contentUri) {
		String realPath = getRealPathFromContentUri(contentUri);
		Uri realUri = Uri.fromFile(new File(realPath));
		
		return realUri;
	}
	
	// get bitmap from filepath
	public static Bitmap decodeFile(String filePath){
        try {
        	File f = new File(filePath);
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 320;

            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        	e.printStackTrace();
        }
        
        return null;
    }
	
	// get Black/White image
	public static Bitmap convertColorBlackWhite(Bitmap orginalBitmap) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
                colorMatrix);

        Bitmap blackAndWhiteBitmap = orginalBitmap.copy(
                Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setColorFilter(colorMatrixFilter);

        Canvas canvas = new Canvas(blackAndWhiteBitmap);
        canvas.drawBitmap(blackAndWhiteBitmap, 0, 0, paint);

        return blackAndWhiteBitmap;
    }
	
	// get brighness image
	public static Bitmap convertBrightness(Bitmap src, int brightnessLevel){
		ColorMatrix cm = new ColorMatrix(new float[] {
	                 1, 0, 0, 0, brightnessLevel,
	                 0, 1, 0, 0, brightnessLevel,
	                 0, 0, 1, 0, brightnessLevel,
	                 0, 0, 0, 1, 0
	        	});

	    Bitmap ret = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());

	    Canvas canvas = new Canvas(ret);

	    Paint paint = new Paint();
	    paint.setColorFilter(new ColorMatrixColorFilter(cm));
	    canvas.drawBitmap(src, 0, 0, paint);

	    return ret; 
	}
	
	// get sepia image
	public static Bitmap convertSepia(Bitmap sampleBitmap){
		ColorMatrix sepiaMatrix =new ColorMatrix();
		float[] sepMat = {
				0.3930000066757202f, 0.7689999938011169f, 0.1889999955892563f, 0, 0,
				0.3490000069141388f, 0.6859999895095825f, 0.1679999977350235f, 0, 0,
				0.2720000147819519f, 0.5339999794960022f, 0.1309999972581863f, 0, 0,
				0, 0, 0, 1, 0,
				0, 0, 0, 0, 1
		};
		
		sepiaMatrix.set(sepMat);
		
		final ColorMatrixColorFilter colorFilter= new ColorMatrixColorFilter(sepiaMatrix);
		
		Bitmap rBitmap = sampleBitmap.copy(Bitmap.Config.ARGB_8888, true);
		Paint paint=new Paint();
		paint.setColorFilter(colorFilter);
		Canvas myCanvas =new Canvas(rBitmap);
		myCanvas.drawBitmap(rBitmap, 0, 0, paint);
		
		return rBitmap;
	}
}